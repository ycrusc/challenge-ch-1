import java.util.Scanner;

public class HitungLuasDanVolume {

    public static void main(String[] args) {
        int terpilih;
        Scanner scanner = new Scanner(System.in);
        do {
           mainMenu();
           terpilih = scanner.nextInt();
            switch (terpilih) {
                case 1:menuDatar();
                        int pilihan1 = scanner.nextInt();
                        switch (pilihan1) {
                        case 1: persegi();
                                PressEnter();
                                break;
                        case 2: lingkaran();
                                PressEnter();
                                break;
                        case 3: segiTiga();
                                PressEnter();
                                break;
                        case 4: persegiPanjang();
                                PressEnter();
                                break;
                        case 5: break;
                        default: System.out.println("Pilihan tidak ada");
                    }
                    break;
                case 2: menuBangun();
                        int pilihan2 = scanner.nextInt();
                        switch (pilihan2) {
                        case 1: kubus();
                                PressEnter();
                                break;
                        case 2: balok();
                                PressEnter();
                                break;
                        case 3: tabung();
                                PressEnter();
                                break;
                        case 4: break;
                        default: System.out.println("Pilihan tidak ada");
                    }
                    break;
                case 3:System.out.println("              ===== TERIMA KASIH =====");
                    break;
                default:
                    System.out.println("Pilihan tidak ada");
                }
            } while (terpilih != 3);
        }

        public static void mainMenu () {
            System.out.println("        ||=====================================||");
            System.out.println("        ||KALKULATOR PENGHITUNG LUAS DAN VOLUME||");
            System.out.println("        ||=====================================||");
            System.out.println("        ||                 MENU                ||");
            System.out.println("        ||1. Hitung Luas Bidang Bangun Datar   ||");
            System.out.println("        ||2. Hitung Volume Bangun Ruang        ||");
            System.out.println("        ||3. Tutup Aplikasi                    ||");
            System.out.println("        ||=====================================||");
            System.out.print("              Pilihan : ");
            }

        public static void menuDatar () {
            System.out.println("=====================================");
            System.out.println("PILIH BIDANG DATAR AKAN DIHITUNG");
            System.out.println("=====================================");
            System.out.println("1. Luas Persegi");
            System.out.println("2. Luas Lingkaran");
            System.out.println("3. Luas Segitiga");
            System.out.println("4. Luas Persegi Panjang");
            System.out.println("5. Kembali ke menu sebelumnya");
            System.out.println("=====================================");
            System.out.print("Pilihan : ");
        }

        public static void menuBangun () {
            System.out.println("=====================================");
            System.out.println("PILIH BANGUN RUANG YANG AKAN DIHITUNG");
            System.out.println("=====================================");
            System.out.println("1. Volume Kubus");
            System.out.println("2. Volume Balok");
            System.out.println("3. Volume Tabung");
            System.out.println("4. Kembali ke menu sebelumnya");
            System.out.println("=====================================");
            System.out.print("Pilihan : ");
        }

        public static void segiTiga () {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Masukkan Alas : ");
            int a = scanner.nextInt();
            System.out.print("Masukkan Tinggi : ");
            int t = scanner.nextInt();
            System.out.println("=====================================");
            System.out.println("Luas Segitiga = " + (a * t) / 2);
        }

        public static void lingkaran () {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Masukkan Jari-jari : ");
            int r = scanner.nextInt();
            System.out.println("=====================================");
            System.out.println("Luas Lingkaran = " + 3.14 * r * r);
        }

        public static void persegi () {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Masukkan Sisi : ");
            int s = scanner.nextInt();
            System.out.println("=====================================");
            System.out.println("Luas Persegi = " + s * s);
        }

        public static void persegiPanjang () {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Masukkan Panjang : ");
            int p = scanner.nextInt();
            System.out.print("Masukkan Lebar : ");
            int l = scanner.nextInt();
            System.out.println("=====================================");
            System.out.println("Luas Segitiga = " + p * l);
         }

        public static void kubus () {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Masukkan Sisi : ");
            int s = scanner.nextInt();
            System.out.println("=====================================");
            System.out.println("Luas Persegi = " + s * s * s);
        }

        public static void balok () {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Masukkan Panjang : ");
            int p = scanner.nextInt();
            System.out.print("Masukkan Lebar : ");
            int l = scanner.nextInt();
            System.out.print("Masukkan Tinggi : ");
            int t = scanner.nextInt();
            System.out.println("=====================================");
            System.out.println("Volume Balok = " + p * l * t);
       }

        public static void tabung () {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Masukkan Jari-jari : ");
            int r = scanner.nextInt();
            System.out.print("Masukkan Tinggi : ");
            int t = scanner.nextInt();
            System.out.println("=====================================");
            System.out.println("Volume Tabung = " + 3.14 * r * r * t);
        }

        public static void PressEnter () {
            System.out.println("Tekan ENTER untuk melanjutkan......");
            try {
                System.in.read();
            } catch (Exception e) {}
        }
    }

